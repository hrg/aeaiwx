<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html; charset=UTF-8" errorPage="/jsp/frame/Error.jsp" %>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html>
<head>
<title>主页面</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<script type="text/javascript" src="js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="js/util.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css">
<style type="text/css">
.footer {
	height: 31px;
	line-height: 31px;
	font-size: 12px;
	text-align: center;
	color: #666666;
	width: 99%;
}
.mainContent{
	font-size: 14px;
	margin:8px 10px;
	line-height:25px;
}

div#wrap {
 padding-top: 87px;
}
</style>
</head>
<body>
<div id="wrap">
<table width="96%"  border="0" align="center">
  <tr>
    <td width="80" align="center"><img src="images/index/mainpic.jpg"></td>
    <td>
<div class="mainContent">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AEAI WX微信扩展框架是基于Java封装的微信公众号二次开发框架，该软件现已开源并上传至开源中国。基于AEAI WX微信扩展框架可以快速接入微信，实现自定义菜单创建、信息按规则自动回复、集成企业的线上系统（HR、CRM、微店、网站等）、同时可以整合集成互联网开放资源（如：百度地图、天气预报、热映电影等）。</div>
<span style="float:left;"><img src="images/index/agileai.jpg" style="height:100px;"></span>
<div class="mainContent">数通畅联公众号也是采用AEAI WX框架来完成的，目前提供：移动办公（微信签到、微信签退、我的工作）、实用功能、商务合作（报价体系、许可升级、手机网站）等菜单，其中，微信签到、签退集成AEAI HR的考情管理功能，我的工作还将整合：我的待办、我的待阅、我的信息、我的客户、我的关注等各业务系统的相关功能。
</div>
<div class="mainContent">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 沈阳数通畅联软件技术有限公司是耕耘于应用系统集成领域的专业技术团队，提供基于Java体系的SOA整合产品，主要包括应用集成平台（AEAI ESB）、门户集成平台（AEAI Portal）、流程集成平台（AEAI BPM）、应用开发平台（AEAI DP，也称Miscdp）、主数据管理平台（AEAI MDM）。其他产品更多详情请参见官网：www.agileai.com,或致电：024-22962011。</div>
</td>
  </tr>
</table>
</div>
</body>
</html>
<script language="javascript">
if (parent.topright.document && parent.topright.document.getElementById('currentPath'))
parent.topright.document.getElementById('currentPath').innerHTML='系统主页面';
</script>
