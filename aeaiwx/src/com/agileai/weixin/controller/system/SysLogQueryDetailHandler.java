package com.agileai.weixin.controller.system;

import com.agileai.hotweb.controller.core.QueryModelDetailHandler;

public class SysLogQueryDetailHandler extends QueryModelDetailHandler{
	public SysLogQueryDetailHandler(){
		super();
		this.serviceId = "systemLogQuery";
	}
}
