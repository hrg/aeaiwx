package com.agileai.weixin.tool;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.weixin.model.Constans.Configs;


public class LocationHelper {
	public static String getAddress(String latitude,String longitude) throws Exception{
		String result = null;
    	CloseableHttpClient httpClient = null;
    	CloseableHttpResponse response = null;
		try {
			String url = "http://api.map.baidu.com/telematics/v3/reverseGeocoding?location="+longitude+","+latitude+"&coord_type=wgs84&output=json&ak="+Configs.BAIDU_KEY;
			HttpGet get = HttpClientManager.getGetMethod(url);    
			httpClient = HttpClientManager.getSSLHttpClient();
	        response = httpClient.execute(get);    
	        String jsonStr = EntityUtils.toString(response.getEntity(), "utf-8");  
	        JSONObject jsonObject = new JSONObject(jsonStr);
	        if (jsonObject != null){
	        	String status = jsonObject.getString("status");
	        	if ("Success".equals(status)){
	        		result = jsonObject.getString("description");
	        	}
	        }	
		} finally{
    		if (response != null){
    			response.close();
    		}
    		if (httpClient != null){
    			httpClient.close();
    		}
		}
		return result;
	}
}
